export const modalReducer = (state, action) => {
  switch (action.type) {
    case "OPEN_MODAL":
      return {
        ...state,
        isActive: action.isActive
      }
    case "CLOSE_MODAL":
      return {
        ...state,
        isActive: action.isActive
      }
    case "SET_MODAL_CONTENT":
      return {
        ...state,
        modalContent: action.modalContent
      }
    case "SET_POKEMON_NAME":
      return {
        ...state,
        pokemonName: action.pokemonName
      }
    default:
      return state;
  }
};