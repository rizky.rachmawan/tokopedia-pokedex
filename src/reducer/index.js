import { pokemonReducer } from './pokemonReducer';
import { modalReducer } from './modalReducer';

const REDUCER = {
  MODAL_REDUCER: modalReducer,
  POKEMON_REDUCER: pokemonReducer,
};

export default REDUCER