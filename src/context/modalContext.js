import React, { createContext, useReducer } from "react";
import REDUCER from '../reducer'

export const ModalContext = createContext();

const initilalValue = {
  isActive: false,
  modalContent: null,
  pokemonName:''
}

const ModalProvider = ({ children }) => {
  const [modal, setModal] = useReducer(REDUCER.MODAL_REDUCER, initilalValue);

  return (
    <ModalContext.Provider value={{ modal, setModal, }}>
      {children}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
