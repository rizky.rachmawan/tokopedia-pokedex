import List from '../../../elements/List';
import ListItem from '../../../elements/ListItem';
import Title from '../../../elements/Title';
import styles from './styles.module.css';

const Evolution = (props) => {
  const { evolutions } = props;
  return (
    <div className={styles.root}>
      <Title heading="h4" className={styles.cusTitle}>
        Evolution Chain
     </Title>
      <List className={styles.cusList}>
        <ListItem>
          {evolutions.map((val, idx) => {
            return val.species_name
          }).join(', ')}
        </ListItem>
      </List>
    </div>
  )
}

export default Evolution;