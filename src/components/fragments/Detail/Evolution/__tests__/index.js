import React from 'react';
import Evolution from '../Evolution';
import renderer from 'react-test-renderer';

describe('Evolution component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      evolutions: [
        {
          species_name: ''
        }
      ]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Evolution {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
