import List from "../../../elements/List";
import ListItem from "../../../elements/ListItem";
import Title from "../../../elements/Title";
import styles from './styles.module.css';

const Move = (props) => {
  const { moves } = props;
  return (
    <div className={styles.root}>
      <Title className={styles.cusTitle} heading="h3">Moves</Title>
      <List className={styles.cusList} >
        {moves.map((val, idx) => {
          return (
            <ListItem key={idx} className={styles.cusListItem}>
              {val.move.name}
            </ListItem>
          )
        })}
      </List>
    </div>
  )
}

export default Move;