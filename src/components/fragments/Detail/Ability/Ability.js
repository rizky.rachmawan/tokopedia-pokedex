import List from '../../../elements/List';
import ListItem from '../../../elements/ListItem';
import Title from '../../../elements/Title';
import styles from './styles.module.css';

const Ability = (props) => {
  const { abilities } = props;
  return (
    <div className={styles.root}>
      <Title heading="h4" className={styles.cusTitle}>
        Abilities
      </Title>
      <List className={styles.cusList}>
        {abilities.map((val, idx) => {
          return (
            <ListItem key={idx}>
              {val.ability.name}
            </ListItem>
          )
        })}
      </List>
    </div>
  )
}

export default Ability;