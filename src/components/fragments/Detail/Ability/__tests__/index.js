import React from 'react';
import Ability from '../Ability';
import renderer from 'react-test-renderer';

describe('Ability component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      abilities: [
        {
          ability: {
            name: '',
          }
        }
      ]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Ability {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
