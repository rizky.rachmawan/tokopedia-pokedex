import React from 'react';
import SpriteImage from '../SpriteImage';
import renderer from 'react-test-renderer';

describe('SpriteImage component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      sprites: [
        {
          front: ''
        },
        {
          back: ''
        }
      ]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<SpriteImage {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
