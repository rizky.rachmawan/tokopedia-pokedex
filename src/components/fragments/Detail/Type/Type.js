import List from '../../../elements/List';
import ListItem from '../../../elements/ListItem';
import Title from '../../../elements/Title';
import styles from './styles.module.css';

const Type = (props) => {
  const { types } = props;
  return (
    <div className={styles.root}>
      <Title heading="h4" className={styles.cusTitle}>
        Type
     </Title>
      <List>
        <ListItem>
          {types.map((val, idx) => {
            return val.type.name
          }).join(', ')}
        </ListItem>
      </List>
    </div>
  )
}

export default Type;