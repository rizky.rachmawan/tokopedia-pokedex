import List from "../../../elements/List";
import ListItem from "../../../elements/ListItem";
import Title from "../../../elements/Title";
import styles from './styles.module.css';

const Stat = (props) => {
  const { stats } = props;
  return (
    <div className={styles.root}>
      <Title heading="h4" className={styles.cusTitle}>
        Base Stat
     </Title>
      <List className={styles.cusList}>
        {stats.map((val, idx) => {
          return (
            <ListItem key={idx}>
              {val.stat.name} : {val.base_stat}
            </ListItem>
          )
        })}
      </List>
    </div>
  )
}

export default Stat;