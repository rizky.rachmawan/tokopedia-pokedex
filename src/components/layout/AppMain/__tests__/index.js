import React from 'react';
import AppMain from '../AppMain';
import renderer from 'react-test-renderer';

describe('AppMain component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(
        <AppMain {...props} />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
