import styles from './styles.module.css';

const ListItem = (props) => {
  const { children ,className} = props;
  const cusStyle = [styles.root,className].filter(Boolean).join(' ')
  return (
    <li className={cusStyle}>
      {children}
    </li>
  )
}

export default ListItem;