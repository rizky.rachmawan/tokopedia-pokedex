import React, { useState } from 'react';
import styles from './styles.module.css';

const Image = (props) => {
  const { url, alt } = props
  const [isLoaded, setIsLoaded] = useState(false);

  const modStyle = isLoaded ? [styles.image, styles.active].join(' ') : styles.image
  const modSkeleton = !isLoaded ? [styles.imageSkeleton, styles.active].join(' ') : styles.imageSkeleton

  return (
    <div className={styles.root}>
      <img src={url} alt={alt} onLoad={() => setIsLoaded(true)} className={modStyle} height="100%" width="100%" />
      <div className={modSkeleton}/>
    </div>
  )
}

export default Image