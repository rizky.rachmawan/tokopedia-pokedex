import styles from './styles.module.css';

const List = (props) => {
  const { children, className } = props;
  const modStlye = [styles.root, className].join(' ')
  return (
    <ul className={modStlye}>
      {children}
    </ul>
  )
}

export default List;