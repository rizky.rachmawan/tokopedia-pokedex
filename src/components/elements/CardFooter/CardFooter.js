import PropTypes from 'prop-types';
import styles from './styles.module.css';

const CardHeader = (props) => {
  const modStyle = [styles.root, props.className].join(' ');
  return (
    <div className={modStyle}>
      {props.children}
    </div>
  )
}

export default CardHeader

CardHeader.defaultProps = {
  children: null,
  className: '',
};

CardHeader.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
