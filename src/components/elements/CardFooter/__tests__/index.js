import React from 'react';
import CardFooter from '../CardFooter';
import renderer from 'react-test-renderer';

describe('CardFooter component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<CardFooter {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
