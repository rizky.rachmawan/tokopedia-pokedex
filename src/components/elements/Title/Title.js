const Title = (props) => {
  const { heading, children, className } = props;
  let element;
  switch (heading) {
    case ('h1'):
      element = <h1 className={className}>{children}</h1>;
      break;
    case ('h2'):
      element = <h2 className={className}>{children}</h2>;
      break;
    case ('h3'):
      element = <h3 className={className}>{children}</h3>;
      break;
    case ('h4'):
      element = <h4 className={className}>{children}</h4>;
      break;
    case ('h5'):
      element = <h5 className={className}>{children}</h5>;
      break;
    case ('h6'):
      element = <h6 className={className}>{children}</h6>;
      break;
    default:
      element = <p className={className}>{children}</p>
  }

  return (
    <div>
      {element}
    </div>
  )
}

export default Title