import PropTypes from 'prop-types';
import styles from './styles.module.css';

const Card = (props) => {
  const modStyle = [styles.root, props.className].join(' ');
  return (
    <div className={modStyle}>
      {props.children}
    </div>
  )
}

export default Card

Card.defaultProps = {
  children: null,
  className: '',
};

Card.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
