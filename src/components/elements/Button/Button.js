import styles from './styles.module.css';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { children, onClick, className, type, disabled } = props;
  const custStyle = [styles.root, className].join(' ')
  return (
    <button onClick={() => onClick()} className={custStyle} type={type} disabled={disabled} >
      {children}
    </button >
  )
}

export default Button

Button.defaultProps = {
  onClick: () => { },
};

Button.propTypes = {
  onClick: PropTypes.func,
};
