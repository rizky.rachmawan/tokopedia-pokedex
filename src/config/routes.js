const routes = {
  HOME: '/',
  POKEMON: '/detail',
  DETAIL: '/detail/:id',
  MY_POKEMON: '/my-pokemon'
};

export default routes;

