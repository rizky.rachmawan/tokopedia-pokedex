import axios from 'axios'
import { URL } from '../../config';

export async function fetchPokemonDetailComplete(id) {
  return await axios.get(`${URL.BASE_URL}pokemon/${id}`)
    .then((detail) =>
      Promise.all([
        detail,
        fetch(detail.data.species.url)
      ])
    )
    .then(([detail, species]) =>
      Promise.all([
        detail,
        species,
        fetch(species.data.evolution_chain.url)
      ])
    )
    .then(([detail, species, evolution]) => {
      return [detail, species, evolution]
    })
}

export async function fetch(url) {
  return await axios(`${url}`)
}
