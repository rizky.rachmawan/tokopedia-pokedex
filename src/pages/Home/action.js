import axios from 'axios'
import { URL } from '../../config';

export function fetchPokemon(offset,limit) {
  return axios(`${URL.BASE_URL}pokemon?offset=${offset}&limit=${limit}`)
}

