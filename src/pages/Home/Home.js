import React, { useState, useEffect, useContext } from 'react';
import Title from '../../components/elements/Title'
import { fetchPokemon } from './action'
import { ROUTES, URL } from '../../config'
import Card from '../../components/elements/Card';
import CardFooter from '../../components/elements/CardFooter';
import CardBody from '../../components/elements/CardBody';
import Image from '../../components/elements/Image';
import styles from './styles.module.css';
import { Link } from 'react-router-dom';
import { PokemonContext } from '../../context/pokemonContext';

const Home = () => {
  const [limit] = useState(30);
  const [isBottom, setIsBottom] = useState(false);
  const { pokemonPersist, dispatch } = useContext(PokemonContext);
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    setOffset(pokemonPersist.listPokemon.length)
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  useEffect(() => {
    if (isBottom || pokemonPersist.listPokemon.length === 0) {
      fetchData();
    }
  }, [isBottom]);

  const fetchData = async () => {
    const result = await fetchPokemon(offset, limit)
    const modResult = handleCheckOwning(result.data.results);
    const listPokemon = [...pokemonPersist.listPokemon, ...modResult]
    dispatch({ type: "LIST_POKEMON", listPokemon });
    setOffset(offset + limit);
    setIsBottom(false)
  };

  const handleCheckOwning = (item) => {
    const myArrayFiltered = item.map((el) => {
      return pokemonPersist.myPokemon.filter((f) => {
        return el.name === f.name
      });
    });
    const modItem = item.map((el, idx) => Object.assign({},
      { ...el, owned: myArrayFiltered[idx].length }
    ))
    return modItem
  }

  const handleScroll = async () => {
    const scrollTop = (document.documentElement
      && document.documentElement.scrollTop)
      || document.body.scrollTop;
    const scrollHeight = (document.documentElement
      && document.documentElement.scrollHeight)
      || document.body.scrollHeight;
    if (scrollTop + window.innerHeight + 50 >= scrollHeight) {
      setIsBottom(true)
    }
  }

  const pokemonList = () => {
    return pokemonPersist.listPokemon.map((val, idx) => {
      return (
        <Link key={idx} to={`${ROUTES.POKEMON}/${val.name}`}>
          <div className={styles['card-container']}>
            <Card className={styles['item-card']}>
              <CardBody>
                <div className={styles['item-content']}>
                  <Image url={`${URL.IMAGE_URL}${idx + 1}.png`} alt={val.name} />
                </div>
              </CardBody>
              <CardFooter className={styles['item-footer']}>
                {val.name}
                <br />owned : {val.owned}
              </CardFooter>
            </Card>
          </div>
        </Link>
      )
    })
  }

  return (
    <div className={styles.root}>
      <Title>
        Pokédex List
      </Title>
      <div className={styles['item-container']}>
        {pokemonList()}
      </div>
    </div>
  )
}


export default Home