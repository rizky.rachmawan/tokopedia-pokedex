import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Layout from './components/layout';
import pages from './pages';
import { ROUTES } from './config'
import ContextProvider from './context';

const App = () => (
  <Router>
    <ContextProvider>
      <Switch>
        <Layout>
          <Route exact path={ROUTES.HOME} component={pages.Home} />
          <Route exact path={ROUTES.DETAIL} component={pages.Detail} />
          <Route exact path={ROUTES.MY_POKEMON} component={pages.MyPokemon} />
        </Layout>
      </Switch>
    </ContextProvider>
  </Router>
);

export default App;
